@extends('layout')
@section('title')
  Portofolio  
@endsection
@section('content')
<!-- ======= My Portfolio Section ======= -->
<section id="portfolio" class="portfolio">
    <div class="container justify-content-center p-5 m-5">

      <div class="section-title">
        <h2 class="text-center text-dark mb-5">My Portfolio</h2>
      </div>

      <ul id="portfolio-flters" class="d-flex justify-content-center">
        <li data-filter=".filter-web">Web</li>
      </ul>

      <div class="row portfolio-container justify-content-center p-5">

        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
          <div class="portfolio-img"><img src=" {{ asset('img/portfolio/porto1.png')}} " class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>Web Profil</h4>
            <p>Web</p>
            <a href="https://gitlab.com/project-uts-web/project-uts" target="_blank" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
          <div class="portfolio-img"><img src=" {{ asset('img/portfolio/porto1.png')}} " class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>Web Sosmed</h4>
            <p>Web</p>
            <a href="https://gitlab.com/milaarsini23/uas-web-login" target="_blank" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

      </div>

    </div>
  </section><!-- End My Portfolio Section -->
</section>
@endsection
