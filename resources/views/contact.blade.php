@extends('layout')
@section('title')
  Contact  
@endsection
@section('content')
    
<!-- ======= Contact Me Section ======= -->
  <section id="contact" class="contact">
   <div class="container p-5 mt-5 mb-5">
     <div class="section-title text-center mb-4 text-secondary">
       <h2>Contact Me on</h2>
     </div>

     <div class="row justify-content-center">

       <div class="col-lg-8 jus">

         <div class="row">
           <div class="col-md-12">
             <div class="info-box">
               <i class="bx bx-share-alt"></i>
               <h3>Social Profiles</h3>
               <div class="social-links">
                 <a href="https://twitter.com/" target="_blank" class="twitter"><i class="bi bi-twitter" title="Menuju profil Twitter" ></i></a>
                 <a href="https://facebook.com/regitapringgandani" target="_blank" class="facebook"><i class="bi bi-facebook"title="Menuju profil Facebook"></i></a>
                 <a href="https://instagram.com/regitapringgandani" target="_blank" class="instagram"><i class="bi bi-instagram "title="Menuju profil Instagram"></i></a>
               </div>
             </div>
           </div>
           <div class="col-md-6">
             <div class="info-box mt-4">
               <i class="bx bx-envelope"></i>
               <h3>Email Me</h3>
               <p>regita@undiksha.ac.id</p>
             </div>
           </div>
           <div class="col-md-6">
             <div class="info-box mt-4">
               <i class="bx bx-phone-call"></i>
               <h3>Call Me</h3>
               <p>+6282146888295</p>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section><!-- End Contact Me Section -->
@endsection
