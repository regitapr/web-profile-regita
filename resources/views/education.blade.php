@extends('layout')
@section('title')
  Education  
@endsection
@section('content')
<!-- Education-->
<section class="resume-section mb-3" id="education">
    <h2 class=" p-5 mt-5 text-center text-secondary">Education</h2>
    <div class="resume-section-content p-2 mt-3 m-3 text-dark">
        <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
            <div class="flex-grow-1">
                <h3 class="mb-0">Universitas Pendidikan Ganesha</h3>
                <div class="subheading mb-3">Undergraduate Program</div>
                <div>Information System</div>
            </div>
            <div class="flex-shrink-0"><span class="text-primary">August 2019 - Coming Soon</span></div>
        </div>
        <hr>
        <div class="d-flex flex-column flex-md-row justify-content-between">
            <div class="flex-grow-1">
                <h3 class="mb-0">SMAN 1 Singaraja</h3>
                <div class="subheading mb-3">Sciences and Mathematics</div>
            </div>
            <div class="flex-shrink-0"><span class="text-primary">July 2016 - July 2019</span></div>
        </div>
        <hr>
        <br>
        <div class="d-flex flex-column flex-md-row justify-content-between">
            <div class="flex-grow-1">
                <h3 class="mb-1">SMPN 1 Singaraja</h3>
            </div>
            <div class="flex-shrink-0"><span class="text-primary">July 2013 - July 2016</span></div>
        </div>
    </div>
</section>
@endsection