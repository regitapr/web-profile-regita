@extends('layout')
@section('title')
  About  
@endsection
@section('content')

    <!-- ======= About Me Section ======= -->
    <section id="about" class="about">
      <div class="container p-5 mb-5 mt-5">
        <div class="section-title text-center text-dark mb-4">
          <h2>About Me</h2>
        </div>

        <div class="row">

          <div class="col-lg-4 d-flex align-items-stretch justify-content-center justify-content-lg-start">
            <img src=" {{ asset('img/about.jpeg') }} " class="img-thumbnail" alt="...">
          </div>
          <div class="col-lg-8 d-flex flex-column align-items-stretch">
            <div class="content ps-lg-4 d-flex flex-column justify-content-center">
              <div class="row">
                <div class="col-lg-6">
                  <ul>
                    <li class="text-dark"><i class="bi bi-chevron-right"></i> <strong>Name:</strong> <span>Regita Pringgandani</span></li>
                    <li class="text-dark"><i class="bi bi-chevron-right"></i> <strong>Website:</strong> <span>www.profil_regita.test</span></li>
                    <li class="text-dark"><i class="bi bi-chevron-right"></i> <strong>Phone:</strong> <span>+62 8214 6888 295</span></li>
                    <li class="text-dark"><i class="bi bi-chevron-right"></i> <strong>City:</strong> <span>Singaraja, Bali, Indonesia</span></li>
                  </ul>
                </div>
                <div class="col-lg-6">
                  <ul class="text-dark">
                    <li><i class="bi bi-chevron-right"></i> <strong>Age:</strong> <span>20</span></li>
                    <li><i class="bi bi-chevron-right"></i> <strong>Degree:</strong> <span>Graduate of SHS</span></li>
                    <li><i class="bi bi-chevron-right"></i> <strong>PhEmailone:</strong> <span>regita@undiksha.ac.id</span></li>
                    <li><i class="bi bi-chevron-right"></i> <strong>Freelance:</strong> <span>Available</span></li>
                  </ul>
                </div>
              </div>
            

            <div class="skills-content ps-lg-4">
              <div class="progress">
                <span class="skill">HTML <i class="val">90%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

              <div class="progress">
                <span class="skill">CSS <i class="val">57%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

              <div class="progress">
                <span class="skill">JavaScript <i class="val">65%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>
    </section><!-- End About Me Section -->
@endsection