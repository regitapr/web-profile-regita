@extends('layout')
@section('title')
  Home  
@endsection
@section('content')
  <!-- ======= Hero Section ======= -->
 
  <section id="hero">
    <br>
    <br>
    <div class="hero-container mt-5 pt-5">
      <img src=" {{ asset('img/me.jpeg')}} " class="img-thumbnail rounded-circle mb-1" alt="Regita">
      <h1 class="text-dark fw-bold" >Regita Pringgandani</h1>
      <br>
      <h2 class="text-secondary">I'm an Information System student in Universitas Pendidikan Ganesha</h2>
      <a href="/contact" class="action-btn btn-warning text-center mt-3 rounded-pill px-3 py-2">Contact Me!</a>
    </div>
  </section><!-- End Hero -->
@endsection

