<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\homeCont::class,'index'])->name('home');

Route::get('home', [App\Http\Controllers\homeCont::class,'index'])->name('home');

Route::get('about', [App\Http\Controllers\aboutCont::class,'index'])->name('about');

Route::get('education', [App\Http\Controllers\educationCont::class,'index'])->name('education');

Route::get('portofolio', [App\Http\Controllers\portofolioCont::class,'index'])->name('portofolio');

Route::get('contact', [App\Http\Controllers\contactCont::class,'index'])->name('contact');
